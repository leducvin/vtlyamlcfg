.. image:: http://gitlab.radonc.hmr/srp/vtlyamlcfg/badges/master/pipeline.svg
   :target: http://gitlab.radonc.hmr/srp/vtlyamlcfg/commits/master

.. image:: http://gitlab.radonc.hmr/srp/vtlyamlcfg/badges/master/coverage.svg
   :target: http://gitlab.radonc.hmr/srp/vtlyamlcfg/commits/master

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
    :target: https://github.com/psf/black


General information
===================

Simple package to read YAML configuration files.

Authors
-------

Vincent Leduc

Features
--------

* Dictionary-like access to config
* Access values using glom (https://glom.readthedocs.io/en/latest/)
* Read-only by default

Documentation
-------------

`http://vleduc.docs.radonc.hmr/vtlyamlcfg <http://vleduc.docs.radonc.hmr/vtlyamlcfg>`_
