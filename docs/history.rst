.. include:: ../HISTORY.rst

Latest Commit
-------------

.. git_commit_detail::
    :branch:
    :commit:
    :sha_length: 10
    :uncommitted:

Git Changelog
-------------

.. git_changelog::
	:revisions: 100
