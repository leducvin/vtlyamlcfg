.. highlight:: shell

============
Installation
============

It is recommended to install ``vtlyamlcfg`` in a virtual
environment.

User mode
---------

If you only want to use ``vtlyamlcfg`` and don't want or need
to modify it, it can be installed with:

.. code-block:: console

    $ pip install --trusted-host=lisa --index-url=http://lisa:8081/simple vtlyamlcfg

This is the preferred method to install ``vtlyamlcfg``, as it
will always install the most recent stable release. Furthermore all dependencies
of ``vtlyamlcfg`` will be installed as well.

The ``--trusted-host`` and ``--index-url`` options can be omitted from the above command if written in the pip config file ``~/.config/pip/pip/conf`` as:

.. code-block:: console

	[global]
	trusted-host = lisa
	index-url = http://lisa:8081/simple

If you want to install the latest commit from the repo, use:

.. code-block:: console

    $  pip install git+http://gitlab.radonc.hmr/srp/vtlyamlcfg.git#egg=vtlyamlcfg

Development mode
----------------

If you think you may need to make modifications to ``vtlyamlcfg``,
you have to install it in development mode.

First, clone the repository:

.. code-block:: console

    $ git clone http://gitlab.radonc.hmr/srp/vtlyamlcfg.git

Packages needed for developping ``vtlyamlcfg`` can be installed
by:

.. code-block:: console

    $ make install-dev


Then, from the cloned repo run:

.. code-block:: console

    $ make devel

From this point any modification made to the cloned repo
``vtlyamlcfg`` will take effect in your virtual environment.

In the case you do make significant modifications, you may consider contributing
your changes to the main codebase.
