PACKAGE = vtlyamlcfg

SHELL = bash

.PHONY: .clean-test .clean-pyc .clean-build .clean-docs .clean-junk clean lint\
	      test test-all coverage coverage-html apidocs docs showdoc servedoc devel\
				release dist install install-dev help


.DEFAULT_GOAL := help


define BROWSER_PYSCRIPT
import os, webbrowser, sys
try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url
webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT


define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))

endef
export PRINT_HELP_PYSCRIPT


BROWSER := python -c "$$BROWSER_PYSCRIPT"


# As we are using setuptools-scm for version tracking,
# use it to find the version from git tags
GITVERSION=$(shell python setup.py --version)


help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)


.clean-junk:
	find . -name '*~' -exec rm -fr {} +
	find . -name '#*#' -exec rm -fr {} +

.clean-build: ## remove build artifacts
	-rm -fr build/
	-rm -fr dist/
	-rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

.clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

.clean-test: ## remove test and coverage artifacts
	-rm -fr .tox/
	-rm -f .coverage
	-rm -fr htmlcov/

.clean-docs: ## remove docs artifacts
	-rm -rf docs/_build

clean: .clean-test .clean-junk .clean-build .clean-pyc .clean-docs ## remove all build, test, coverage, doc and python artifacts


git-init: clean ## initialize git repo
	[[ ! -e .git ]]
	git init
	git add .
	git commit -m "Initial commit"
	git remote add origin http://gitlab.radonc.hmr/srp/$(PACKAGE)
	-pre-commit install
	-pre-commit install -t pre-push


lint: ## check style with flake8
	flake8 src/$(PACKAGE) tests


test: ## run tests quickly with the default python
	pytest -vx tests


test-all: ## run tests on every Python version with tox
	tox


coverage: ## check code coverage quickly with the default python
	coverage run --source=$(PACKAGE) -m pytest
	coverage report --omit='*/_version.py','*/__init__.py' -m


coverage-html: coverage ## run coverage and display report in browser
	coverage html
	$(BROWSER) htmlcov/index.html


apidocs: .clean-docs ## generate api docs with sphinx-apidoc
	# need to manually cleanup for command to take effect
	# rm -f docs/$(PACKAGE).rst;
	# rm -f docs/modules.rst;
	sphinx-apidoc -o docs/ src/$(PACKAGE)
	$(MAKE) -C docs clean
	$(MAKE) -C docs html


docs: .clean-docs ## generate Sphinx HTML documentation
	$(MAKE) -C docs clean
	$(MAKE) -C docs html

showdoc: docs ## display doc in browser
	$(BROWSER) /radonc/dev/docs/html/$(PACKAGE)_$(GITVERSION)/index.html


servedocs: docs ## compile the docs watching for changes
	watchmedo shell-command -p '*.rst' -c '$(MAKE) -C docs html' -R -D .


devel: ## install in development mode
	# python setup.py develop
	pip install -e .


sdist: clean ## build source package
	python setup.py sdist


bdist: clean ## build wheel package
	python setup.py bdist_wheel


install: clean ## install the package to the active python's site-packages
	# python setup.py install
	pip install .


install-dev: ## install development packages
	pip install -r requirements_dev.txt


release: clean bdist ## package and upload a release
	cp dist/*.whl /radonc/dev/packages/wheels
	dir2pi /radonc/dev/packages/wheels

pull-lfs:
	git lfs update
	git lfs pull --exclude= --include="*"
