#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""
from setuptools import find_packages, setup


PACKAGE = "vtlyamlcfg"

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("HISTORY.rst") as history_file:
    history = history_file.read()


# packages needed by this project
requirements = [
    # package from pypi index
    # 'pyodbc',
    #
    # package from local index. Must add
    # [global]
    # extra-index-url = file:///radonc/dev/packages/wheels/simple
    # in ~/.config/pip/pip.conf
    # 'rtobjects'
    "pyyaml>=5.1",
    "glom",
]

setup(
    name=PACKAGE,
    description="Simple module to read YAML configuration files.",
    long_description=readme + "\n\n" + history,
    author="Vincent Leduc",
    author_email="leducvin@posteo.net",
    url="http://gitlab.radonc.hmr/srp/%s" % (PACKAGE),
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    include_package_data=True,
    install_requires=requirements,
    # allows requirements to be installed with "python setup.py install"
    dependency_links=["/radonc/dev/packages/wheels"],
    # uncomment and edit to include package-related data
    # package_data={PACKAGE:['datadir/*']},
    package_data={PACKAGE: ["src/examples/*"]},
    # uncomment and edit to provide commands
    # entry_points={'console_scripts': ['%s=%s:main' % (PACKAGE, PACKAGE)]},
    use_scm_version={"write_to": "src/%s/_version.py" % (PACKAGE)},
    setup_requires=["setuptools_scm"],
)
