"""Top-level package for vtlyamlcfg."""
from __future__ import absolute_import, division, print_function, unicode_literals

from vtlyamlcfg._version import version as __version__  # noqa: F401
from vtlyamlcfg.vtlyamlcfg import *  # noqa: F401, F403


__author__ = "Vincent Leduc"
