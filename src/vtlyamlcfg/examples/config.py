import os

from vtlyamlcfg import YAMLConfig


"""
An example of how yaml_config can be used.
"""

# FileNotFoundError does not exist in python 2.
# We substitute for IOError.
try:
    FileNotFoundError
except NameError:
    FileNotFoundError = IOError


class _MyConfig(YAMLConfig):

    """
    A helper object for reading a YAML configuration file.

    Define any methods or properties to help you conveniently access
    information from your config.yaml file.
    """

    def _filter_rooms_by_model(self, model):
        return [r for r in self["mlc_model"] if self["mlc_model"][r] == model]

    @property
    def bm_rooms(self):
        """Get a list of Beam Modulator machines.

        Returns:
            list of int: the list of Beam Modulator machine numbers.
        """
        return self._filter_rooms_by_model("beammodulator")

    @property
    def mlci_rooms(self):
        """Get a list of MLCi machines.

        Returns:
            list of int: the list of MLCi machine numbers.
        """
        return self._filter_rooms_by_model("mlci")

    @property
    def agility_rooms(self):
        """Get a list of Agility machines.

        Returns:
            list of int: the list of Agility machine numbers.
        """
        return self._filter_rooms_by_model("agility")

    def get_leaf_width(self, machine):
        """Get leaf width for a machine."""
        return self["leaf_width"][self["mlc_model"][machine]]

    def get_ref_leaf(self, machine):
        """Get the reference leaf number for a machine."""
        return self["ref_leaf"][self["mlc_model"][machine]]


#  If your config.yaml file is placed in the same directory as this
#  file, you will be able to do:
cfg = _MyConfig()
_thisdir = os.path.dirname(os.path.realpath(__file__))
try:
    cfg.read("{}/config.yaml".format(_thisdir))
except FileNotFoundError:
    # Don't forget to call config.read(path) if your config.yaml file
    # is not located in the same directory as this file.
    pass
