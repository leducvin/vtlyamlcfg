"""Main module."""

import pprint

from glom import glom
import yaml


class YAMLConfig(object):

    """
    A simple object used to read a YAML configuration file.

    Behaves much as a python dictionary. You can subclass and add any
    methods.

    Set the ``read_only`` attribute to ``False`` prior to editing config
    values.
    """

    def __init__(self, path=None, unsafe=False):
        self._cfg = {}
        self.read_only = True
        if path:
            self.read(path, unsafe=unsafe)

    def glom(self, *args, **kwargs):
        """Glom the config using :py:func:`glom`."""
        return glom(self._cfg, *args, **kwargs)

    def read(self, path, unsafe=False):
        """
        Read and load a YAML file located at ``path``.

        Returns ``self``.
        """
        with open(path, "r") as yamlfile:
            if unsafe:
                self._cfg = yaml.unsafe_load(yamlfile)
            else:
                self._cfg = yaml.full_load(yamlfile)
        return self

    def copy(self):
        """Get a copy of the config."""
        c = self.__class__()
        c.read_only = self.read_only
        c._cfg = self._cfg.copy()
        return c

    def get(self, item):
        return self._cfg.get(item)

    def __setitem__(self, item, value):
        if self.read_only:
            raise AttributeError(
                "read_only attribute is set to True. Set it to false if you"
                " want to dynamically edit the configuration."
            )
        self._cfg[item] = value

    def __getitem__(self, item):
        return self._cfg[item]

    def __repr__(self):
        return f"<{type(self).__name__}: {pprint.pformat(self._cfg)}>"

    def __call__(self, *args, **kwargs):
        """Shortcut to glom method."""
        return self.glom(*args, **kwargs)

    def __iter__(self):
        return iter(self._cfg)

    def update(self, other):
        """
        Update the configuration dictionary with another dictionary.

        Returns ``self``.
        """
        if self.read_only:
            raise AttributeError(
                "read_only attribute is set to True. Set it to false if you"
                " want to dynamically edit the configuration."
            )
        self._cfg.update(other)
        return self


cfg = YAMLConfig()
