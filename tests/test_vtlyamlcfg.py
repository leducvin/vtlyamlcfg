#!/usr/bin/env python

import os

import pytest

from vtlyamlcfg import cfg


"""Tests for `yaml_cfg` package."""


_thisdir = os.path.dirname(os.path.realpath(__file__))


def test_cfg():
    cfg.read("{}/../src/vtlyamlcfg/examples/config.yaml".format(_thisdir))
    assert cfg["nominal_px_sp"][1] == 0.401

    with pytest.raises(AttributeError):
        cfg["leaf_width"] = 0

    cfg.read_only = False
    cfg["leaf_width"] = 0
    assert cfg["leaf_width"] == 0

    d = {"toto": 1}
    cfg.update(d)
    assert cfg["toto"] == 1

    assert cfg.glom("ref_leaf.agility") == 40
    print(cfg)

    _cfg = cfg.copy()
    assert _cfg.glom("ref_leaf.agility") == 40
